import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrustedQuoteComponent } from './trusted-quote.component';

describe('TrustedQuoteComponent', () => {
  let component: TrustedQuoteComponent;
  let fixture: ComponentFixture<TrustedQuoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrustedQuoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrustedQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
