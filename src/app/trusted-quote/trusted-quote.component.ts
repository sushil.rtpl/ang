import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-trusted-quote',
  templateUrl: './trusted-quote.component.html',
  styleUrls: ['./trusted-quote.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TrustedQuoteComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
