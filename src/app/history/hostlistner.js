https://angular-2-training-book.rangle.io/handout/advanced-angular/directives/listening_to_an_element_host.html
@HostListener('window:scroll', ['$event'])
stickyForm(event) {
    let scrollAmount = this.document.body.scrollTop || this.document.documentElement.scrollTop;
    let clientWindowWIdth = 500;
    let clientWindowHeight = 500;

    if (isBrowser) {
        clientWindowWIdth = this.document.documentElement.clientWidth || this.document.body.clientWidth;
        clientWindowHeight = this.document.documentElement.clientHeight || this.document.body.clientHeight;
    }


    if (scrollAmount > (this.enquiryblock.nativeElement.offsetTop + this.sliderheight.nativeElement.clientHeight) && clientWindowWIdth > 819) {
        if (scrollAmount > ((this.footersection.nativeElement.offsetTop) - this.enquirecontainer.nativeElement.clientHeight)) {
            this.isSticky = false;
            this.isBottomSticky = true;
        } else {
            this.isSticky = true;
            this.isBottomSticky = false;
        }

    } else if (this.isSticky && scrollAmount < (this.enquiryblock.nativeElement.offsetTop + this.sliderheight.nativeElement.clientHeight)) {
        this.isSticky = false;
    }
}
// View Child
@ViewChild('enquiryblock') enquiryblock: ElementRef;
@ViewChild('sliderheight') sliderheight: ElementRef;
@ViewChild('footersection') footersection: ElementRef;
@ViewChild('enquirecontainer') enquirecontainer: ElementRef;