import { Component, OnInit } from '@angular/core';

// import {
//   trigger,
//   state,
//   style,
//   animate,
//   transition
// } from '@angular/animations';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
  // animations: [
  //   trigger('heroState', [
  //     state('inactive', style({
  //       backgroundColor: '#eee',
  //       transform: 'scale(1)'
  //     })),
  //     state('active',   style({
  //       backgroundColor: '#cfd8dc',
  //       transform: 'scale(1.1)'
  //     })),
  //     transition('inactive => active', animate('100ms ease-in')),
  //     transition('active => inactive', animate('100ms ease-out'))
  //   ])
  // ]
})
export class HistoryComponent implements OnInit {
  // state : any = "inactive";
  constructor() { }

  ngOnInit() {
  }
  // toggle(value){
  //   debugger;
  //   if(value=="inactive"){
  //     this.state = "active"
  //   }else{
  //     this.state = "inactive"
  //   }
   
  // }
}
