import { Component, OnInit } from '@angular/core';
import { Ng2ScrollableDirective } from 'ng2-scrollable';
import { scrollTo } from 'ng2-utils';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }


  // Smooth Scroll
  id = 'menu'; 
  // hid = 'h1'; wid='w1';
  scrollTo(selector, parentSelector, horizontal) {
    scrollTo(
      selector,       // scroll to this
      parentSelector, // scroll within (null if window scrolling)
      horizontal,     // is it horizontal scrolling
      0               // distance from top or left
    );
  }
  test(){
    alert("Hello");
  }
  
  scrollEvent(e){
    console.log('e',e);
  }

}
