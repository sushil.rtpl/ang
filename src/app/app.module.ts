import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// Animation
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// Routing
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

// Font Awesome
import { AngularFontAwesomeModule } from 'angular-font-awesome';
// Bootstrap
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NavComponent } from './nav/nav.component';
import { BannerComponent } from './banner/banner.component';
import { HistoryComponent } from './history/history.component';
import { SpecialComponent } from './special/special.component';
import { ReservationsComponent } from './reservations/reservations.component';
import { FeatureComponent } from './feature/feature.component';
import { OurMenuComponent } from './our-menu/our-menu.component';
import { TrustedQuoteComponent } from './trusted-quote/trusted-quote.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { FooterComponent } from './footer/footer.component';
import { LandingComponent } from './landing/landing.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { BlogComponent } from './blog/blog.component';
import { ErrorComponent } from './error/error.component';






@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BannerComponent,
    HistoryComponent,
    SpecialComponent,
    ReservationsComponent,
    FeatureComponent,
    OurMenuComponent,
    TrustedQuoteComponent,
    SubscribeComponent,
    FooterComponent,
    LandingComponent,
    ContactComponent,
    AboutComponent,
    BlogComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,    
    AppRoutingModule,
    AngularFontAwesomeModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot()

    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
// export class AppBootstrapModule {}
