import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SubscribeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
